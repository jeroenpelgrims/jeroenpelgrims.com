---
title: "Updated SoundSwitch"
description: SoundSwitch updated
date: 2011-06-23T23:39:00
slug: updated-soundswitch
taxonomies:
  tags:
    - soundswitch
    - projects
    - software-development
---

<br>
<a href="http://res.cloudinary.com/dxo5rc6wd/image/upload/v1385070588/arrow_switch_ypzixg.png"><img class="alignleft size-full wp-image-645" title="arrow_switch" src="http://res.cloudinary.com/dxo5rc6wd/image/upload/v1385070588/arrow_switch_ypzixg.png" alt="" width="32" height="32" /></a>

I updated my <a href="http://soundswitch.codeplex.com/">SoundSwitch</a> Application.
You can install it by going <a href="http://soundswitch.codeplex.com/releases/">here</a>.

The purpose of SoundSwitch is to be able to easily switch between 2 or more Audio output devices.
For example, I have a wireless headset and my regular speakers.
Both are detected as a separate audio card in the sound panel.
The only way to switch between them is to open the playback devices panel and select one of them and choose to put it as default device.

That's such a hassle!
With SoundSwitch you just press Ctrl+Alt+F11 and it toggles between the cards you selected.

Todos:

1. Let the application start up when the user logs in. (currently you have to start it yourself)
1. Allow users to set their own shortcut (instead of Ctrl+Alt+F11)
1. Make the configuration easier. (Won't be possible I'm afraid though)

**UPDATE:**  
Changed the hosting to CodePlex, apparently you can host ClickOnce applications there!
