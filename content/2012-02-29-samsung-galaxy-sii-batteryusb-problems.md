---
title: "Samsung Galaxy SII battery/USB problems"
date: 2012-02-29T20:39:00
slug: samsung-galaxy-sii-batteryusb-problems
taxonomies:
  tags:
    - android
---

## Problem description

I recently had some nasty trouble with my Galaxy SII.
The problem I was having was that after working normally the phone would suddenly act like I was pulling out and putting in the usb cable constantly.  
The battery would indicate being charged, discharged and then charged again (etc.).
After shutting the phone down and trying to reboot it I would get an image showing the big battery image you get when charging the phone while it's off.
A few seconds later it would also display an exclamation mark and a thermometer.

I tried letting the battery "cool" to no avail (It didn't feel exceptionally warm to the touch anyway)  
Switching to a different battery did seem to fix the problem, at least for a short while. A day later I was having the exact same symptoms. Time for some more Googling I thought.

## Solution

Eventually I found [this topic](http://androidforums.com/samsung-galaxy-s2-international/377331-spurious-battery-temperature-warning.html) on Android Forums. The solution to my page was found on [page 2](http://androidforums.com/samsung-galaxy-s2-international/377331-spurious-battery-temperature-warning-2.html#post3386729).  
Apparently even a small amount of dirt in the micro usb port can cause these problems.
It's easily cleaned with a toothpick. To get in the thin space away from the battery you can split a toothpick with scissors.
You can dip the tip of the toothpick in rubbing alcohol to get even the last bits of dirt.

If even this doesn't work and you're sure it's the usb port (water damage maybe) the port itself is quite easily and cheaply (€15) replaced.
In the same topic [someone did this](http://androidforums.com/samsung-galaxy-s2-international/377331-spurious-battery-temperature-warning-2.html#post3934910) and posted [an image](http://blog.upsheep.tk/wp-content/uploads/2012/02/galaxyS2-module.jpg) of the port's package showing all the product info.

Also posted was a video showing how to disassemble and reassemble the phone.

<iframe frameborder="0" height="360" src="http://www.youtube.com/embed/QKztg1ra4-0" width="480"></iframe>
