---
title: "Pushing to Dokku from GitLab CI"
date: 2016-03-05T02:01:00
slug: pushing-to-dokku-from-gitlab-ci
taxonomies:
  tags:
    - git
    - dokku
    - software-development
---

<div class="cover">![GitLab CI logo](/images/gitlab-ci-logo.png)</div>

I had heard about [GitLab](https://gitlab.com) for a while ago but it was only recently that I actually tried it out a bit more thorougly and discovered they have quite a few cool features.  
Just like [Bitbucket](https://bitbucket.org) they have a free tier which includes private repositories, or projects in GitLab lingo.

But on their free solution on gitlab.com —the .org domain is for the project itself— you can also make use of their CI service [GitLab CI](https://about.gitlab.com/gitlab-ci/). Making use of the CI service is as easy as adding a file called `.gitlab-ci.yml`.  
This makes the CI usage so much faster and easier than configuring everything on a separate site like [CodeShip](https://codeship.com) or [Semaphore](https://semaphoreci.com). Another big benefit is that there are no limitations of how many builds you can do per month or restrictions on private projects.

My projects are hosted on my own Dokku setup and I needed to find a way to automatically push from my GitLab repo to my Dokku server (using ssh authentication) on a successful build by GitLab CI.

## Getting Gitlab CI to push to Dokku

### Generating an SSH keypair

```bash
$ ssh-keygen -t rsa
Enter file in which to save the key (/home/<username>/.ssh/id_rsa): /home/<username>/.ssh/example
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/<username>/.ssh/example.
Your public key has been saved in /home/<username>/.ssh/example.pub.
```

For the rest of the tutorial I'll assume you left the passphrase empty.

### Adding the keypair to Dokku [#](https://www.digitalocean.com/community/questions/dokku-add-new-ssh-key?answer=9187)

`sshcommand` should already be installed on your server by Dokku by default.

```bash
$ cat /home/<username>/.ssh/example.pub | ssh root@<yourdokkuinstance> "sudo sshcommand acl-add dokku <description>"
```

Where `<yourdokkuinstance>` is the domain or ip where your dokku is running.  
It'll show a bunch of hexadecimal numbers separated by parentheses here if successful.

### Adding the private key to GitLab

Print out the contents of your private key and copy them to your keyboard.

```bash
cat /home/resurge/.ssh/example
```

Copy everything from  
`-----BEGIN RSA PRIVATE KEY-----`  
to  
`-----END RSA PRIVATE KEY-----`

- Open GitLab.
- Go to the project you want to deploy
- Open the settings
- Open the Variables tab
- Add a new variable with the key `SSH_PRIVATE_KEY` and paste the contents of the private key file.

### Using the private key in the GitLab CI script

Create a file called `.gitlab-ci.yml` in your project repository.

Add the following contents:

```yaml
before_script:
  - mkdir -p ~/.ssh
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
  - chmod 600 ~/.ssh/id_rsa
  - ssh-keyscan -H '<yourdokkuinstance>' >> ~/.ssh/known_hosts

stages:
  - deploy

deploy_to_dokku:
  stage: deploy
  script:
    - git push dokku@<yourdokkuinstance>:<projectname> master
```

### Useful info: Removing the key from Dokku when it's obsolete

To remove the key you added earlier to the server where your Dokku installation is running you can SSH into the server and run the following command:  
`sshcommand acl-remove dokku <description>`  
Where `<description>` is the description you entered at the `sshcommand acl-add` step.

## Other Paas providers

Most of the steps in this post should be easily usable for other Paas providers as well like Heroku or OpenShift.  
You'll have to ignore step 2 (Adding the keypair to Dokku) and possibly step 1 as well (Generating an SSH keypair). Instead, you should attach the generated key to your repository or account on your Paas provider's site or it's possible you'll be provided with one (and thus don't need to generate one).

This post was [deployed using Gitlab CI](https://gitlab.com/jeroenpelgrims/jeroenpelgrims.com/builds) as described in the post above :)
