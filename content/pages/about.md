---
title: About
description: A description on what jeroenpelgrims.com is about and a look into the life of Jeroen.
path: about
---

## This site

### The content

This is the personal website & blog of Jeroen Pelgrims, a Belgian [independent sofware developer](https://digicraft.eu) & teacher living and working in Antwerp.
This blog will contain writings on things that I want to explain clearly. The main purpose being so I can look back on it later to refresh how I solved that particular problem or to link to other people who might be helped by that information.

There's no real restriction on the content that you'll find here but since I'm quite often busy with things involving software development the majority of the content will most likely be of that nature.

### The technology side

The site itself is a statically generated site built with [Zola](https://www.getzola.org). This build is then deployed to [Cloudflare pages](https://pages.cloudflare.com/).  
I used to have a whole self-hosted setup where I would build a docker image with the generated site inside, that image would then be hosted in Gitlab's container repository and then pulled down to my home server by using [watchtower](https://github.com/containrrr/watchtower).  
Which was a fantastic learning experience, but simpler is better. Saves a bit on electricity as well.

Code published on this site falls under the MIT license unless specified otherwise.

## Myself

### Brief history

I was born and raised in [Antwerp, Belgium](http://www.openstreetmap.org/relation/59518), the best city _in the world_.  
At the [Karel De Grote University College](https://www.kdg.be) (KdG) I studied applied computer science and after that started my professional career as a software developer.

Living most of your life in the same 10 square kilometers seemed a bit claustrophobic for me.
So after a few years working in Belgium I made the move to live and work abroad. I've spent a wonderful 4,5 years in [Stavanger, Norway](https://www.openstreetmap.org/node/30421326#map=14/58.9636/5.7371); which I now consider to be my second home town.  
When you live in a country for a long while you should learn it's language I think. So now I speak Dutch, English, Norwegian and a bit of French.

I've since moved back to my birth place, Antwerp, where I actually took a meander for a few years career-wise.  
For 3 years I was a high school teacher at [Scheppersinstituut Deurne & Antwerpen](https://scheppers.be/). Here I taught basic computer skills and helped maintain the school's IT coordination.  
While teaching there I graduated with a bachelors in Secondary Education, also from KdG.

### Interests

In my spare time I'm still quite active in software development. I love testing out new programming languages and libraries that come along. I'm particularly interested in web applications using HTML, Typescript and node.js.

Besides that I go hitchhiking now and then. I have about 5000km under my belt now. Still an amateur compared to some of the other people I've met though. Related to this, I'm also interested in camping and regular hiking.

More regular interests include board games, computer games, gardening, having a chat and drink with friends.  
But in general I like to try out anything that interests me.  
I've dabbled with:

- Growing my own yeasts from fruit and flour
- Brewed beer, cider and baked bread using these yeasts
- Used old pallet wood to build my own planters and a chicken coop

## My presence on the internet

Here links to some of my profiles on well known websites.

### Professional

- [Linkedin](https://www.linkedin.com/in/jeroenpelgrims)

### Programming

- [GitLab](https://gitlab.com/jeroenpelgrims)
- [Github](https://github.com/jeroenpelgrims)
- [Bitbucket](https://bitbucket.org/resurge)
- [Stack Overflow](http://stackoverflow.com/users/16720/jeroen-pelgrims)

### Projects

My projects have been moved [here](/projects/)

### Various

- [Twitter](https://twitter.com/@jeroenpelgrims)
- <a rel="me" href="https://toot.community/@jeroenpelgrims">Mastodon</a>
- [Facebook](https://facebook.com/jeroenp87)
- [Instagram](https://instagram.com/jeroenpelgrims/)
- [Goodreads](https://www.goodreads.com/user/show/26541474-jeroen-pelgrims)
- [IMDB](https://www.imdb.com/user/ur16478303/ratings)
- [Reddit](https://reddit.com/u/resurge)
- [Last.fm](http://www.last.fm/user/Resurge)
- [Hacker News](https://news.ycombinator.com/user?id=resurge)
