---
title: Projects
description: Some showcased projects made by Jeroen Pelgrims
path: projects
---

## [SoundSwitch](https://github.com/Belphemur/SoundSwitch)

This small piece of software allows you to quickly switch between playback devices by using a key combination (on Windows).  
I'm the original author, this is now being further developed by [Antoine Aflalo](https://aaflalo.me/)  
[Source code](https://github.com/Belphemur/SoundSwitch)

## Computer skills courses for high school (in Dutch)

Not all courses are equally finished

- [Algoritmisch denken](https://books.jeroenpelgrims.com/books/algoritmisch-denken)
- [Presentaties maken](https://books.jeroenpelgrims.com/books/online-cursus-presentaties-maken)
- [Word processing](https://books.jeroenpelgrims.com/books/online-cursus-tekstverwerking)
- [Excel](https://books.jeroenpelgrims.com/books/online-cursus-excel)

## [Anki Chinese](https://chinese.jeroenpelgrims.com/)

You input some Chinese characters and this tool will generate the matching pinyin, stroke order and sound files.  
I use this to create cards for [Anki](https://apps.ankiweb.net/).  
[Source code](https://gitlab.com/jeroenpelgrims/anki-chinese-ts)

## [BulkTTS](http://bulktts.jeroenpelgrims.com/)

BulkTTS is a tool I wrote to quickly transform words to mp3 files.  
These I would then also use in Anki decks to train my vocabulary in other languages.  
[Source code](https://gitlab.com/jeroenpelgrims/bulktts)

## [Recipes](https://recipes.jeroenpelgrims.com)

I wanted a version-controlled way to store my favourite recipes and a simple way to share them with friends.  
[Source code](https://gitlab.com/jeroenpelgrims/recipes)
