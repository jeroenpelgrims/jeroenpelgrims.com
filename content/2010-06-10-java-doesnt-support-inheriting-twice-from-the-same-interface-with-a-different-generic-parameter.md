---
title: "Java doesn't support inheriting twice from the same interface with a different generic parameter"
date: 2010-06-10T04:30:00
slug: java-doesnt-support-inheriting-twice-from-the-same-interface-with-a-different-generic-parameter
taxonomies:
  tags:
    - java
    - software-development
---

I created this:

```java
public class Observable <T> {
  private List<Observer<T>> observers = new ArrayList<Observer<T>>();

  public void addObserver(Observer<T> observer){
    observers.add(observer);
  }

  public void removeObserver(Observer<T> observer){
    observers.remove(observer);
  }

  public void removeAllObservers(){
    observers.removeAll(observers);
  }

  public void notifyObservers(Observable<T> observable){
    for(Observer<T> observer : observers){
      observer.update(observable);
    }
  }
}
```

And this:

```java
public interface Observer <T> {
  void update (Observable<T> observable);
}
```

So I could do this:

```java
public class SomeClass implements Observer<SomeObservableA>, Observer<SomeObservableB>{
  @override
  public void update(Observable<SomeObservableA> observable){
    //do stuff for Observable A
  }

  @override
  public void update(Observable<SomeObservableB> observable){
    //do stuff for Observable B
  }
}
```

But apparently java only takes into account the class you're implementing and not the extra generics you're adding.
So I ended up doing it this way (using the standard Observer and Observable):

```java
public class SomeClass implements Observer{
  @Override
  public void update(Observable o, Object arg) {
    if(o instanceof SomeObservableA){
      //do stuff for Observable A
    }

    if(o instanceof SomeObservableB){
      //do stuff for Observable B
    }
  }
}
```
