---
title: "Learning Norwegian - Some resources"
description: A list of available resources to learn Norwegian
date: 2011-04-11T01:03:00
slug: learning-norwegian-some-resources
taxonomies:
  tags:
    - education
    - norway
---

![Norwegian flag graphic](./norway_grunge_flag.jpg)
<sub class="muted">Norwegian flag in grunge style by [Gabriel Oliveira](http://think0.deviantart.com)</sub>

With only 5 million speakers it's not very easy to find resources on learning Norwegian compared to other languages. Listed below is a compilation of the resources I found while looking for learning materials.  
Norwegian is the first language I'm consciously learning on my own, not in a school environment. (I say consciously because I learned English by watching a load of movies and series, I didn't really have to make an effort then) I'm currently a complete beginner in Norwegian with only 25/30 Pimsleur lessons done and some extra vocabulary with Anki.

## Textbooks

Full fledged learning tools containing a textbook explaining grammar and spelling rules, audio tapes for pronunciation and often also workbooks to apply the acquired knowledge.

### [Pimsleur Norwegian][pimsleur_norwegian_amazon]

30 audio lessons of 30 minutes each. I started learning with this. After each lesson of Pimsleur's completed I could clearly notice that I had learned something. The rate at which I learned stuff using Pimsleur is very high. Unfortunately Pimsleur Norwegian has only 1 level of lessons, compared to 2 or 3 levels for larger languages like French or Chinese. If I had to suggest a starting point to learning Norwegian, this would be it.  
[Pimsleur Norwegian website][pimsleur_norwegian_site]

[pimsleur_norwegian_amazon]: http://www.amazon.com/gp/product/0743566300/ref=as_li_ss_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=0743566300
[pimsleur_norwegian_site]: http://www.pimsleur.com/Learn-Norwegian

### [Ny I Norge][ny_i_norge_amazon] &amp; [Bo I Norge][bo_i_norge_amazon]

Ny I Norge contains a textbook, audio readings of the conversations in the textbook, a workbook and listening comprehension exercises. Bo I Norge is a textbook for more advanced learners (after Ny I Norge). This will be the second resource I'll be using, after Pimsleur.

[ny_i_norge_amazon]: http://www.amazon.com/gp/product/8211001233/ref=as_li_ss_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=8211001233
[bo_i_norge_amazon]: http://www.amazon.com/gp/product/8211000121/ref=as_li_qf_sp_asin_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=8211000121

### [Colloquial Norwegian][colloquial_norwegian]

Similar to Ny I Norge, consists of a textbook with exercises and audio readings. I do have the impression that the learning curve of the book is higher.

[colloquial_norwegian]: http://www.amazon.com/gp/product/0415110114/ref=as_li_qf_sp_asin_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=0415110114

### [På vei][på_vei_site], [Stein På Stein][stein_på_stein_site], [Her På Berget][her_på_berget_site]

All three books consist of a textbook, a workbook and audio files. They consist of three levels of which På Vei is the 1st, Stein På Stein the second and finally Her På Berget the third.  
Each of the books have free exercises online. ([På vei](http://pavei.cappelen.no), [Stein På Stein](http://steinpastein.cappelendamm.no), [Her på berget](http://herpaberget.cappelendamm.no))

[på_vei_site]: http://www.cappelendammundervisning.no/undervisning/project-detail.action?id=7030
[stein_på_stein_site]: http://www.cappelendammundervisning.no/undervisning/project-detail.action?id=1962
[her_på_berget_site]: http://www.cappelendammundervisning.no/undervisning/project-detail.action?id=7045

### Praktisk Norsk [I][praktisk_norsk_1_product_site] &amp; [II][praktisk_norsk_2_product_site]

From the same series as På vei etc. Also has free exercises [online here][praktisk_norsk_1_site] and [here][praktisk_norsk_2_site].

[praktisk_norsk_1_product_site]: https://www.cappelendammundervisning.no/vare/praktisk-norsk-1-%282013%29-9788202403249
[praktisk_norsk_2_product_site]: https://www.cappelendammundervisning.no/vare/praktisk-norsk-2-(2013)-9788202403256
[praktisk_norsk_1_site]: http://praktisknorsk1.cappelendamm.no
[praktisk_norsk_2_site]: http://praktisknorsk2.cappelendamm.no

### [Teach Yourself Norwegian][teach_yourself_norwegian_amazon]

A book with conversations both in English and in Norwegian. Seems to rely heavily of cross referencing the dialogues in both languages. Also contains audio recordings for the conversations.

[teach_yourself_norwegian_amazon]: http://www.amazon.com/gp/product/0340887575/ref=as_li_qf_sp_asin_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=0340887575

### [Sett i gang I &amp; II](http://norwords.com/textbook/index.html)

A textbook, workbook and audio files. The audio files are freely downloadable after creating an account on the website.

### [My Little Norway](http://mylittlenorway.com/learn-norwegian/)

A blog about 2 people in Norway, one native speaker and one who's learning Norwegian. They also have a bunch of lessons on the blog for learning Norwegian.

### Assimil (for German or French speakers)

[Norwegisch ohne Mühe][german_resource_amazon] (For German speakers) | [Assimil site][assimil_site]  
[Le Norvegien sans Peine][french_resource_amazon] (For French speakers)

[german_resource_amazon]: http://www.amazon.com/gp/product/3896252100/ref=as_li_qf_sp_asin_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=3896252100
[assimil_site]: http://www.assimil.com/descriptionProduitDetail.do?paramIdProduit=1441&paramIdMethode=1441
[french_resource_amazon]: http://www.amazon.com/gp/product/0320068196/ref=as_li_qf_sp_asin_tl?ie=UTF8&tag=perused-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=0320068196

## Television

### [NRK](http://www.nrk.no/nett-tv/)

NRK is the Norwegian national broadcaster. A lot of the programs are visible freely (Click A-Å on the top and choose a starting letter of a program). For example, [the news][nrk_news] is something that is updated daily.  
UPdate: SinceTwriting this post the NRK nett-tv site has changed. You can find a list of programs available for viewing outside of Norway [here][nrk_programs_abroad].

[nrk_news]: https://tv.nrk.no/serie/nyheter
[nrk_programs_abroad]: http://tv.nrk.no/programmer/utland

### [TV3](http://www.tv3play.no/program)

TV3 broadcasts more foreign (to Norway) programs which are more limited by drm. But those are most likely not the programs you want to watch anyways cause they are in different languages :)  
Watch out, they also have some Swedish programs.

### [TVNorge](http://webtv.tvnorge.no) (NOT FREE)

### [TV2](http://www.tv2.no/play/) (SEMI-FREE)

Only short clips of programs are free, full programs require a subscription.

## Radio

- [Radio Norge](http://www.radionorge.com)
- [NRK P1](http://www.nrk.no/p1/)
- [NRK P2](http://www.nrk.no/p2/)
- [NRK Petre](http://p3.no/) (P3)
- [NRK Alltid nyheter](https://radio.nrk.no/direkte/alltid_nyheter) (always news) - Click "Nettradio" in the left bar.
- [NRK Klassisk](https://radio.nrk.no/direkte/klassisk)
- [P4 Radio Hele Norge](http://www.p4.no/) - Click "lytt nå"
- [All NRK Radio channels](http://www.nrk.no/radio/)
- [A huge list of other norwegian radio channels](http://www.radiolist.net/norway)

## Extra resources

### [Anki flashcards](http://ankisrs.net/)

Anki is a program to exercise flashcards using <a href="http://en.wikipedia.org/wiki/Spaced_repetition">spaced repetition</a>. I use this to exercise vocabulary. The great thing about Anki is that you can also add audio files to your flashcards, so you can train pronunciation and spelling in one go.  
I don't like having to record every word manually, so I made <a href="https://bulktts.jeroenpelgrims.com/">this site</a> to automatically generate sound files for words you type in.  
[Here][frequency_list_norwegian] is a list of the 1000 most used Norwegian words to get you started. Be sure to use [Bokmål][bokmål_nynorsk_wikipedia] (see below).

[frequency_list_norwegian]: http://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/Norwegian
[bokmål_nynorsk_wikipedia]: http://en.wikipedia.org/wiki/Bokmaal_and_Nynorsk#Bokm.C3.A5l_and_Nynorsk

### News in "Simple Norwegian"

<a href="http://www.klartale.no/">Klar Tale</a>is a news website written in simple Norwegian. If you use their website together with a browser plugin which can translate (unknown) words you select then this is another good way of learning.

### Lessons on Youtube

<a href="http://www.youtube.com/user/Crienexzy">Crienexzy</a> on Youtube has a bunch of videos on Norwegian pronunciation.

### Films

A great way to passively (but slowly) learn is by watching movies and series. Some Norwegian films I liked were:

<ul>
  <li><a href="http://www.imdb.com/title/tt1255956/">Vegas</a></li>
  <li><a href="http://www.imdb.com/title/tt1740707/">Trolljegeren</a></li>
  <li><a href="http://www.imdb.com/title/tt1278340/">Død snø</a></li>
  <li>Fritt Vilt <a href="http://www.imdb.com/title/tt0808276/">I</a>, <a href="http://www.imdb.com/title/tt1188990/">II</a>, <a href="http://www.imdb.com/title/tt1464535/">III</a></li>
</ul>
A big list of Norwegian films can be found <a href="http://www.imdb.com/search/title?title_type=feature&primary_language=no&sort=moviemeter,asc&ref_=tt_dt_dt">on IMDB</a>.

### <a href="http://www.livemocha.com/">Livemocha</a>

Livemocha uses the same teaching style as Rosetta Stone. I'm not particularly fond of this way of learning, I learned a bunch more by just a few lessons of Pimsleur. However, it's a great way to find native speaking partners.

### [Duolingo](https://en.duolingo.com/)

Duolingo offers a course of Norwegian online for free as well. It's a pretty well known language learning site which uses gamification in it's learning process.  
Maybe you have a friend learning a different language on the site? Then you can compete with each other to learn the most.

### <a title="Interpals" href="http://interpals.net">Interpals</a>

A site aimed at bringing penpals together. Has quite a large user base.

### Linguistic tools &amp; sound files of Norwegian dialects

<a href="http://www.hf.ntnu.no/nos/list.php">This site</a> contains sound files of texts in various Norwegian dialects.

### More info

<a href="http://norwegianlanguage.info/index.html">This site</a>has a bunch of links to Norwegian related things.

## Bokmål &amp; Nynorsk

Norwegian has 2 written forms, <a href="http://en.wikipedia.org/wiki/Bokmaal_and_Nynorsk#Bokm.C3.A5l_and_Nynorsk">Bokmål and Nynorsk</a>. Unlike what Nynorsk's (new-Norwegian) name seems to suggest it is actually older than Bokmål. The grand majority of (newly) written material is in Bokmål, so you'll have the most use of learning Bokmål. As far as I know, all textbooks listed above use Bokmål.

## Get started

There's a torrent floating around somewhere containing a lot of these learning resources. You can find it by searching for "Norwegian - learning resources".

Credit where credit is due: a lot of the resources mentioned here came from <a href="http://www.reddit.com/r/Norway/comments/gfh4v/id_love_to_learn_the_basics_of_norwegian_where/c1n70ok">this post</a> on reddit. <span style="color: #ff0000;">  
<strong>Update</strong></span> Found <a href="https://web.archive.org/web/20101104172332/http://www.stolaf.edu/depts/norwegian/grammar/index.html">a site</a> with good explanations on grammar rules. And <a title="Verbix conjugations" href="http://www.verbix.com/languages/norwegian.shtml">a site</a> which will show you ALL conjugations of a given verb! Super useful.

[magnet_url]: magnet:?xt=urn:btih:ccc7c3224ad88d5686d2c379e542879fe9f38d12&dn=norwegian+learning+resources&tr=udp%3A%2F%2Ftracker.istole.it%3A80%2Fannounce&tr=udp%3A%2F%2Fopen.demonii.com%3A1337
