---
title: "Using custom Fontawesome Kit icons in React"
description: How to use custom Fontawesome Kit icons in React and NextJS
date: 2023-04-13T20:37:00
slug: custom-fontawesome-kit-icons-in-react
taxonomies:
  tags:
    - react
    - software-development
---

I had a project where we needed to use Fontawesome icons and to do this the [@fortawesome/react-fontawesome](https://www.npmjs.com/package/@fortawesome/react-fontawesome) library was a great help.
But at some point the client needed icons that weren't in FontAwesome's icon library. Neither the free or Pro icons.

Not a problem I thought, I knew it was a possibility to upload icons to FontAwesome and use those custom icons together with FontAwesome's own icons through something called "FontAwesome kits".  
But to my surprise you can't easily use those custom icons in react through `react-fontawesome`.

I needed a way to easily combine the standard icons with my custom uploaded ones.  
I found [this StackOverflow thread](https://stackoverflow.com/questions/73933932/using-an-icon-uploaded-to-a-kit-with-fontawesome-react/75980519) on the topic, which wasn't very helpful since the official answer from FontAwesome was:

> Right now, custom icons can only be used in pro kits and kits cannot be used in React. We are looking into supporting more ways to use kits in the future.

So I tried to build my own workaround.

### 1. Upload the custom icons to a FontAwesome kit

You can only upload icons to a FontAwesome kit.  
A kit is a specific icon configuration for a project or website.  
You'll need a [FontAwesome pro](https://fontawesome.com/plans) account to be able to upload icons to a kit.

### 2. Include the kit script

The custom uploaded icons are only accessible through a FontAwesome kit so you'll need to use that kit somewhere in your project.  
To do this, include the kit's script tag somewhere in your project.  
This looks something like this:

```
  <script src="https://kit.fontawesome.com/<kit-id>.js" crossorigin="anonymous"></script>
```

Replace `<kit-id>` with your kit's id. You can find this snippet on your FontAwesome profile.

For a NextJS project you could place this in the `_document.js` file.  
But instead of a regular `script` tag you should use the `next/Script` component or you'll get some warnings.

### 3. A custom component to use both standard & custom icons

As it isn't possible to include custom icons through the `FontAwesomeIcon` component of the `@fortawesome/react-fontawesome` library we'll need to make a component ourselves.  
Ideally this component can be used to include both standard FontAwesome icons and custom icons we uploaded.

```tsx
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Here I use the names of the icons I've added on Fontawesome's website.
type CustomIconName = "my-icon-1" | "my-icon-2" | "my-icon-3";

// A type to indicate we'll be using a custom icon instead of a
// standard FontAwesome icon
type CustomIconDefinition = {
  prefix: "fa-kit";
  iconName: CustomIconName;
};

// a Union type so we can use this component both for
// standard icons and custom icons.
export type KitIconDefinition = IconDefinition | CustomIconDefinition;

type Props = {
  icon: KitIconDefinition;
};

export default function FontAwesomeKitIcon({ icon }: Props) {
  if (icon.prefix === "fa-kit") {
    return <i className={`fa-kit fa-${icon.customIconName}`} />;
  } else {
    return <FontAwesomeIcon icon={icon} />;
  }
}
```

### 4. Using the custom component

#### We can still use a standard FontAwesome icon

```tsx
import { faStar } from "@fortawesome/pro-regular-svg-icons";
...
<FontAwesomeKitIcon icon={faStar}/>
```

#### We can use one of our custom uploaded icons

```tsx
const faMyIcon1 = { prefix: "fa-kit", iconName: "my-icon-1" };
...
<FontAwesomeKitIcon icon={faMyIcon1} />
```

Note that the iconName property refers to the `CustomIconName` type, which should only contain names of custom uploaded icons.

#### Mix & match standard and custom icons.

As our `FontAwesomeKitIcon` component supports both standard and custom icon definitions we can use these interchangeably.

```tsx
import { faStar, faBoat } from "@fortawesome/pro-regular-svg-icons";
import { myIcon1, myIcon2 } from "./myIconDefinitions";

const icons = [
  faStar,
  myIcon1,
  faBoat,
  myIcon2
];
...
icons.map(icon => <FontAwesomeKitIcon icon={icon} />)
```

## The code
You can check out an example of the code [here](https://github.com/jeroenpelgrims/custom-fontawesome-kit-icons-in-react)