---
title: "Bachelor in Applied Computer Science"
description: My thoughts on getting my bachelor's diploma in Applied computer science and my plans for the future
date: 2010-06-30T00:44:00
slug: bachelor-in-applied-computer-science
taxonomies:
  tags:
    - education
    - personal
    - software-development
---

Yesterday evening I received my diploma.

This post isn't in line with the previous posts I made but it _is_ in line with the general content of this blog, things I learned.

When I entered college I had a basic knowledge of programming. I knew some PHP and some VB. These past three years I learned a bunch, possibly more than I thought I would learn when entering college.

## Onwards in education!

I am now preparing myself to further my education.
The next two years I'll be working for my master's degree, also in Applied Computer Science.
I am not yet officially signed up for a University yet --signups start begin July-- but I'm pretty sure I'll be attending [Vrije Universiteit Brussel (VUB)](https://www.vub.be).

### Why more?

I've gotten a lot of questions as to _why_ I want to go for a master's degree, especially from the people I've worked with in my internship at <a href="http://www.itemsolutions.com/">Item Solutions</a> (of which the majority are also <a href="http://www.kdg.be/">Karel de Grote</a> Alumni).

The main reason is simple, I want to learn more. Some specifics include:

- Functional Programming
- Machine Learning
  - Evolutionary programming
  - Genetic algorithms/programming
  - Neural networks
- Algorithms to compare images
- The principles of optimization (O(n) etc.)
- Secret Santa problem

The Secret Santa problem was something I read about while participating in the <a href="http://redditgifts.com/">Reddit Secret Santa</a>. This is a very specific problem I'm mentioning here because that's what I wrote down in my "want to learn list" I created in the course of last year. But what I actually want to learn is more general of course, think <a href="http://programmingpraxis.com/">every problem on Programming Praxis</a>.
The site is often described as having less "mathy" problems than <a href="http://projecteuler.net/">Project Euler</a>, yet I can only sporadically solve one of them, and most of the time in an as brute-forced-way as possible.

Another reason I want to get my master's degree is because I'm interested in living abroad for a short period of time (1-2 years) and from what I've read countries are a lot easier to get in to for a long period of time when you have a degree. Now, I don't know what the difference is between a Master's and a Bachelor's degree when it comes to getting in to countries but I assume the higher the degree, the easier it is to get in.

In the last three years I've noticed I also enjoy explaining things to classmates. So I've also thought about a possible future as a college professor, in CS related fields of course.

And finally a reason that is completely not connected to computers: Since VUB is quite a bit further from home I'll need to have a "kot" (student home/dorm). I believe this is a great stepping stone to becoming more independent.
