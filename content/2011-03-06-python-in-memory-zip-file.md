---
title: "Python in-memory zip file"
description: This post describes creating a zip file in-memory using Python.
date: 2011-03-06T13:16:00
slug: python-in-memory-zip-file
taxonomies:
  tags:
    - python
    - software-development
---

I needed this for something where I wanted to send the visitor of a website a bunch of files he selected at once.
An easy way to do this would be to add the files to a zip file and then send that zip file to the user.
Unfortunately Python doesn't have an in-memory zip file library, you can only interact with zip files on disk.

After a bit of googling around I came to this [StackOverflow answer][stackoverflow_answer].
That worked like a charm, and here is my more reusable version:

```python
from zipfile import ZipFile
from StringIO import StringIO

class InMemoryZipFile(object):
    def __init__(self):
        self.inMemoryOutputFile = StringIO()

    def write(self, inzipfilename, data):
        zip = ZipFile(self.inMemoryOutputFile, 'a')
        zip.writestr(inzipfilename, data)
        zip.close()

    def read(self):
        self.inMemoryOutputFile.seek(0)
        return self.inMemoryOutputFile.getvalue()

    def writetofile(self, filename):
        open('out.zip', 'wb').write(self.read())
```

Save as `inmemoryzip.py` and import it as `inmemoryzip`.
This is quite limited compared to the standard ZipFile class, but this gets the job done for what I needed it.

[stackoverflow_answer]: http://stackoverflow.com/questions/3610221/how-to-create-an-in-memory-zip-file-with-directories-without-touching-the-disk/3616796#3616796
