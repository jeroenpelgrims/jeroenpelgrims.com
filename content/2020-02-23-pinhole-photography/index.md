---
title: "Pinhole photography"
description: Photo galleries of my first two attempts at pinhole photography using a self made camera.
date: 2020-02-23T01:15:00
slug: pinhole-photography
taxonomies:
  tags:
    - photography
---

A while back I experimented a bit with pinhole photography, something that had interested me since I had heard about it by a hitchhiker I had met at a hitchhiking meeting.
He ([Clemens Schmid](https://twitter.com/ClemensSchmid7)) had made a camera out of cardboard and in this camera was a regular 35mm film roll.
About 6 years after seeing that camera in use I finally made one myself. And not much after that I made a second one!

## The first camera

{{ gallery(folder="1") }}

## The second camera

{{ gallery(folder="2") }}

## Plans

The plans I used for the first camera were the ones of the so called "Dippold pinhole camera".
These can be found here: [https://francescocapponi.tumblr.com/dippold](https://francescocapponi.tumblr.com/dippold)

The second camera was just a bit of a random tryout.
I had a nice looking tea bin which I had which I thought could make a nice looking camera.
But the pinhole size and focal length aren't quite great.

### Other pinhole resources:

- [http://www.mrpinhole.com/](http://www.mrpinhole.com/)
