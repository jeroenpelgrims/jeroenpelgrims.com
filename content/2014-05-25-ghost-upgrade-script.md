---
title: "Ghost upgrade script"
date: 2014-05-25T17:22:00
slug: ghost-upgrade-script
taxonomies:
  tags:
    - linux
    - software-development
    - bash
---

The following is a bash script to upgrade your Ghost installation automatically to the latest version.
The script is meant to be used on an Arch linux installation but should be pretty generic. On other distro's you should only have to change the lines to start/stop ghost. (see "Stopping Ghost" & "Starting Ghost" below)

**Important:** This does _not_ update your template, in case you've made modifications to it yourself. This will still have to be done manually.

```bash
#!/bin/bash

BACKUP_FILE=/tmp/backup.tgz
GHOST_UPDATE_URL=https://ghost.org/zip/ghost-latest.zip

if [ "$1" == "" ]; then
    echo "Arg 1 must be the path to the blog folder!"
    exit 1
fi
BLOG_PATH=$1

echo 'Stopping Ghost'
sudo systemctl stop <servicename>

echo 'BACKUP starting'
tar -capf $BACKUP_FILE -C $BLOG_PATH .
echo 'BACKUP done'

{
    LATEST_FOLDER=/tmp/ghost-latest

    echo 'Downloading latest Ghost version'
    wget -q $GHOST_UPDATE_URL -P /tmp/
    rm -rf $LATEST_FOLDER
    unzip -q /tmp/ghost-latest.zip -d $LATEST_FOLDER

    echo 'Overwriting root files'
    (
        cd $LATEST_FOLDER;
        cp *.js *.json *.md LICENSE $BLOG_PATH
    )

    echo 'Replacing core directory'
    rm -rf "$BLOG_PATH/core"
    cp -r "$LATEST_FOLDER/core" "$BLOG_PATH/core"

    echo 'Installing Ghost dependencies'
    (
        cd $BLOG_PATH;
        npm install --production --quiet
    )

    echo 'Upgrade done!'
} || {
    echo 'BACKUP restore'
    rm -rf $BLOG_PATH
    mkdir $BLOG_PATH
    tar -xf $BACKUP_FILE -C $BLOG_PATH
    echo 'BACKUP restore done'
}

echo 'Starting Ghost'
sudo systemctl start <servicename>
```
