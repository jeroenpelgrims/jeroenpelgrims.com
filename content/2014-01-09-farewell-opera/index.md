---
title: "Farewell Opera"
date: 2014-01-09T20:15:00
slug: farewell-opera
taxonomies:
  tags:
    - personal
---

![Opera browser logo](./Opera-logo.png)

About 10-something years ago I was introduced to Opera by a friend of mine. Yes, that was when Opera's free version still came with a banner ad but that didn't stop me from loving the browser.

Some of the features I liked and eventually even started to regard as _essential_ for a browser are:

- Tabs on multiple lines
- Mouse "rocker" features. (holding of RMB and clicking LMB would navigate back in history and holding LMB and clicking RMB would navigate forward)
- More tab management and navigation: Ctrl-W, double/middle clicking tab bar to open new tab, backspace to navigate backwards (Really, Chrome and FF still don't have this by default!?)
- F2: being able to quickly enter a new url to browse to
- Everything I wanted was included in the Opera installer. I didn't need to fiddle around with any addons, plugins or extensions.
- custom search keywords. Typing `i The Time Machine` in the address bar would cause the browser to go to imdb.com and search for "The Time Machine" automatically. The shortcuts are customizable. (In my case w for wikipedia, wn for the Dutch wikipedia, d for dictionary.com, ...)
- Fast startup and immediately available. After opening Opera the address bar was focused enabling me to type what I wanted and pressing enter. (I remember this not being the case for older versions of other browsers)

I admit, some of these are more habits than features other browsers don't have.  
Examples that come to mind are F2 in Opera vs F6 in Firefox/Chrome and various plugins for FF with the same functionality of some Opera features.

## Opera Blink

Unfortunately a while ago Opera Software chose to discontinue work on Presto, their layout engine, instead to fork the Webkit project.  
Initially I didn't see a big problem in this, most likely because I didn't know how extensive the impact would be on the browser. This would also not have been a problem as long as the browser would have still been the same in essence.

### Shortcomings

But it was not. Right after the release of Opera 15, the first version of the browser based on Blink, it was clear there was still a long way to go before it would be possible to "upgrade" without losing some features Opera 12.16 had (Last version with Presto).  
Even now, [half a year after the release][opera 15 release date] of the "new" Opera, a lot is still missing.

### Opera 12.x getting out of date

I myself was still using the last Presto version but lately it has become unbearable to use. There were huge memory leaks in the application. 4 tabs would consume about 1 GB of memory easily and when leaving your pc on for the night and coming back the day after the memory usage would have crept up to 1.6-2 GB and would cause the program to become unresponsive with the smallest of usage.

While Opera once had the fastest javascript engine (Carakan), leaving even the major browsers in it's wake, currently it's lagging behind because the lack of updates to 12.x.  
It was also a forerunner in implementing W3C standards but currently a lot of features are just not fully there.  
Even though it's not that widely used yet, WebGL is a big gaping hole.

## An alternative to my beloved Opera

### Firefox

Especially pre-Chrome a lot of preople were raving about Firefox. As with the well known catchphrase "there's an app for that" there was also "an addon for that" in Firefox.  
I was never a big fan though.  
As I said before I liked that with Opera I could install it and know that everything I wanted was right there. No need to remind myself of what addon I used to fix that problem.

### Chrome

The same went for Chrome. When it was released it was the fastest browser there was but without any sugar to it, it was very simple.  
A while later extensions were added. But this left me with the same concerns as I had with Firefox addons.

My fear of addon configuring have become moot though since both Chrome and Firefox have a feature where you can sync your Addon (settings) provided you have a Google account or an account with a server which supports Firefox Sync.

### My choice

As you see I've only listed these 2 options here because I'm looking for something cross platform and you can be pretty sure that these 2 will always be up-to-date with the latest new things.

Why Firefox over Chrome? Well it's a case of disliking the direction Google is going and liking Mozilla's vision on things concerning the web.

#Final words
Seeing from when I've started I've been a big fan of Opera for quite some time now.
I've always laughed with Firefox copying a lot of features of Opera shortly after they were released, but now I'm glad they did. This made the switch that much easier.

I've set up the addon syncing and it seems to be working fine accross pcs excluding syncing the addon's settings although I've read this is a responsability of the addon's authors.

Now let's just hope I'll [stay satisfied with Firefox][reddit firefox ui].

[opera 15 release date]: http://en.wikipedia.org/wiki/History_of_the_Opera_web_browser#Version_15
[reddit firefox ui]: http://www.reddit.com/r/technology/comments/1unig0/not_cool_mpaa_joins_the_w3c/cekexhd?context=3
