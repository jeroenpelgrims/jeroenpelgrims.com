---
title: "Build-it-yourself NAS and why not to use a Raspberry PI for it"
date: 2023-10-18T20:42:00
slug: diy-nas
taxonomies:
  tags:
    - diy
    - nas
    - crafts
    - software-development
---

## Current setup
Currently my home server setup consists of a [10 year old laptop](https://en.wikipedia.org/wiki/Lenovo_Yoga_2_Pro).  
It's a pretty janky setup since the laptop doesn't even have an ethernet port, so it's connected to my network through a USB-to-ethernet cable.
For data storage I'm using a 4TB external harddrive, also connected through a USB cable.

The 4TB data drive is getting a bit cramped so I'm looking to upgrade both in storage volume as stability.

![Photo of my old laptop being a good server](./home-server.jpg)

> The drive is in the cardboard box below the laptop.
> Those network cables at the bottom of the photo? They go to my router which is dangling from them.  
> Did I mention my current setup is a bit janky?


## Prefab is expensive
When you go looking for ready-made NAS devices you'll probably bump into either a Synaptic or QNAP device.  
For a 5+bay device you'll quickly end up paying at least [500 EUR][qnap_syna_price] just for the device itself without any drives.  

That's quite a bit of money that could better be used on the drives themselves, or at least to get better hardware for the price.  
So I started looking for possibilities to build a NAS myself.  

[qnap_syna_price]: https://tweakers.net/nas/vergelijken/#filter:PY5BC8IwDIX_S84V1uGY9gcMdvDUo3iobdRIt5a0iDj2381APOXly3uPLHBlN4cxFDDn475TWreHiwKaX8jVPlK2GNFXSjOYm4sFFSQOyANhDGAgMz0L_KBNXIW54v8kox_Ft9MKsrujpQ-C6Rq1BT2eSGplKWIbKFZk-WOBtu_1NqftDJ2UTe4tSjctrOv6BQ

## Raspberry PI

My first thought was why not use a Raspberry PI 4 as the core hardware?  
It has a very low power consumption and the processor & ram are still good enough when comparing with a prefab solution.

But when actually trying to configure everything I'd need to build a NAS using a PI I came accross quite some downsides.

### No direct SATA connection
The drives can only be connected to the PI through the USB ports that are on it.  
I know HATs are a thing, but they're not easily available and often still quite expensive.

### Not enough (high speed) ports.
The PI has 4 USB ports in total, but only 2 of them are USB 3.0 ports.  
USB 2.0's max speed is 480 Mbps (60 MB/s), which is significantly slower than the speed of modern hard drives.  
This means it will be nigh to impossible to connect more than 2 data drives to the PI.

### Can't check S.M.A.R.T. data of the drives.
Hard drives can do a periodic self-check to see if there are any indications of issues.  
This can give you some extra time to find a solution before the drive fails.  
That's a pretty important feature to have in a NAS.  
But it's apparently impossible to use this feature if the drives are [connected through USB](https://superuser.com/questions/154337/is-it-possible-to-check-an-external-usb-drive-health-smart).

### Too much power being drawn through the PI
We could connect all the drives to the PI using a USB to SATA cable, but that would mean that the PI would need to supply power to all those drives.  
It wouldn't be impossible, but it would make it hard to upgrade to more drives later if we wanted to.

### Boot Drive corruption
Usually the SD card is used as a boot drive for a RPi.  
But SD card corruption in a RPi is a known issue. It can happen by a sudden power loss or even just writing to the card too often.  

A solution for this could be to use an SSD as a boot drive and connect it to the PI using USB.  
But seeing that's again a USB-&gt;SATA protocol conversion and a power draw on the PI that doesn't seem like a super stable solution either.  
And I would like to keep a certain level of stableness for the new NAS.

Plus I've actually tried getting a PI to boot from USB before and it definitely isn't as easy to set up as many guides lead you to believe.

### Conclusion: Fickle
All the downsides above, in addition to the many *dongles* make the RPi solution seem quite fickle.

## Low-power PC configuration 
I started thinking I needed to go back to something that uses some more standard desktop hardware.  
Like a simple motherboard with a modern laptop CPU on it to keep it low power.

Initially I was looking at Atom CPUs, but eventually I somehow bumped into Intel's new [N series][n_series] and the [N100][N100] in particular.  
It's low-power with a TDP of 6 watts and [double to quadruple][cpu comparison] the processing power of any of the prefab NAS'es (at a lower wattage!).

I couldn't find a way to buy the cpu by itself, it seems you need to buy it preassembled to a motherboard.  
The one I liked most was the [AsRock N100DC-ITX][N100DC-ITX].

This board gives me 2 SATA connectors, 1 M.2 connector for the boot drive and a PCIe 3.0 x4 slot.  
All the issues I had with the fickleness of the RPi are solved with this.  
The PCIe slot also allows me to get a SATA controller to increase the total supported (data) drives to 8 if I wanted to.

[n_series]: https://ark.intel.com/content/www/us/en/ark/products/series/231819/intel-processor-n-series.html 
[N100]: https://ark.intel.com/content/www/us/en/ark/products/231803/intel-processor-n100-6m-cache-up-to-3-40-ghz.html
[cpu comparison]: https://www.cpubenchmark.net/compare/5157vs2960vs3667vs4472/Intel-N100-vs-Intel-Celeron-J3355-vs-Intel-Celeron-J4125-vs-Intel-Celeron-N5095

## Component list

Based on that motherboard I looked at what else I needed for hardware.  
Below you can find an overview of all the elements that went into the NAS:

| Component | Name | Price |
| - | - | - |
| Motherboard+CPU | [N100DC-ITX][N100DC-ITX] | € 139.90 |
| RAM | [G.Skill Aegis F4-3200C16S-16GIS][ram] | € 39.90 |
| M.2 SSD | [Samsung 980 500GB][m.2] | € 42.90 |
| HDD 1 | [Toshiba MG09 18TB][toshiba] | € 277.00 |1
| HDD 2 | [Seagate Exos X18 18TB][seagate]  | € 292.31  | 
| 19v adapter | Seasonic SSA-0601D 19V - 60W | € 23.99 |
| Sata power splitter | [Akasa SATA power splitter 0.3m][sata power splitter] | € 6.66 |
| Case | [Inter-Tech IT-607][case] | € 42.71 |
| Sata controller (optional) | [Axagon PCES-SA6][sata-controller] | € 67.06 | 
| | | |
| | Total | € 932.43 |

[N100DC-ITX]: https://www.asrock.com/mb/Intel/N100DC-ITX/
[ram]: https://www.gskill.com/product/165/185/1567584071/F4-3200C16S-16GIS 
[m.2]: https://www.samsung.com/us/computing/memory-storage/solid-state-drives/980-pcie-3-0-nvme-gaming-ssd-500gb-mz-v8v500b-am/ 
[toshiba]: https://storage.toshiba.com/enterprise-hdd/cloud-scale-capacity/mg09-series 
[seagate]: https://www.seagate.com/be/nl/products/enterprise-drives/exos-x/x18/ 
[sata power splitter]: https://akasa.co.uk/search.php?seed=AK-CBPW05-30
[case]: https://www.inter-tech.de/productdetails-156/IT-607_DESKTOP_EN.html
[sata-controller]: https://www.axagon.eu/en/produkty/pces-sa6

Compared to a prefab NAS I'm saving about 100-150 EUR, and I get more powerful hardware in return.

### Why 2 different hard drives?
[It is said][myth] that it's not a good idea to buy multiple of the same drives from the same brand at the exact same time. They could be drives that are manufactured in the same batch.  
The production parameters (and defects) that a drive has will also determine how long a drive has before it will eventually fail.

If drives are from the same batch their production parameters are the same, and thus so is their approximate time left to live. This means that the chance is higher that these drives will fail at times that are close together.  
Which is exactly what you don't want if you need time to copy the data to a replacement drive :) 

Buying drives from a different manufacturer alleviates this problem somewhat.  
Drives with different production parameters will (hopefully) also have different "death times".

[myth]: https://community.spiceworks.com/topic/880874-don-t-get-drives-from-the-same-batch

### Why no power supply/why the laptop adapter?
This motherboard has a 19v plug like a laptop. The motherboard then powers the other connexted devires like the harddrives.   
As long as there's only 2 HDDs we don't require a separate power supply like in a desktop.  

### The case
I tried to get something that was as compact as possible.  
Seeing I'll only have 2 drives for now this one fits perfectly.  
I am thinking about buying some extra fans & dust covers for the fans though, by default the case
only comes with an 80mm fan on the side.

I'm fairly happy with the case, the only part which was a bit dissapointing was the incorrect information of the amound of 3,5" slots.
It's supposed to have 2 of those slots, and there's definitely enough place for it in the case, but the screw holes are definitely not matching with how they are on the drives.  
In the end I had to buy a 5,25" to 3,5" slot converter.

## Operating system

I don't want to go too deep into this in this post, but you have a few options here.  
The most well known ones are probably:  

- [OpenMediaVault](https://www.openmediavault.org/)
- [TrueNAS](https://www.truenas.com/)
- [Unraid](https://unraid.net/)

I went with OpenMediaVault since it seemed to have all the features I needed.  
I tested it out and it seemed to be quite easy to get started with.


