const element = document.querySelector("[data-decode]");
const href = element.getAttribute("href");
const encoded = href.match(/mailto:(.*)/)[1];
const email = atob(encoded);
element.setAttribute("href", `mailto:${email}`);
element.innerHTML = email;
